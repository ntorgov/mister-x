function getAllLinks() {
	var listOfLinks = document.getElementsByTagName('a');
	var results = [];
	if (listOfLinks.length > 0) {
		for (index = 0; index < listOfLinks.length; index++) {
			var currentTestElement = listOfLinks[index];
			var hrefAttribute = currentTestElement.getAttribute('href');
			if (hrefAttribute.match(urlPattern)) {
				var completteUrl = baseUrl + hrefAttribute;
				results.push(completteUrl);
			}
		}
	}
	return results;
}

function someFunction(link) {
	console.log(link);
}
// Called when the user clicks on the browser action.

chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
	if (changeInfo.status == 'complete') {
		var currentLocation = tab.url;
	
		if(currentLocation==satellite1 || currentLocation==satellite2 || currentLocation==satellite3) {
			if(currentLocation==satellite1) {
				chrome.tabs.sendMessage(tab.id, { text: "toSat3" });
			}
			if(currentLocation==satellite3) {
				chrome.tabs.sendMessage(tab.id, { text: "toSat1" });
			}
		}
	
		if(currentLocation.match(urlPattern)) {
			chrome.tabs.sendMessage(tab.id, { text: "WriteDescription" });
		}
	
		chrome.tabs.sendMessage(tab.id, { text: "report_back" }, function handler(response){
			if(typeof response != "undefined"){
				var unOpened = response.length;
				if(unOpened>0) {
					
					chrome.tabs.getAllInWindow(null, function(tabs){
					
						for(var unOpenedIndex = 0; unOpenedIndex < unOpened; unOpenedIndex++) {
							var needToOpen = true;
							for (var i = 0; i < tabs.length; i++) {
								if(tabs[i].url == response[unOpenedIndex]) {
									needToOpen = false;
								}
							}
							if(needToOpen) {
								chrome.tabs.create({ url: response[unOpenedIndex] });
								console.log("Open" + response[unOpenedIndex]);
							}                      
						}
					});
					console.log(response);
				}
			}
		});

	}
});
