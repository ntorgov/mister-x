var baseUrl = 'http://mister-x.me';
var mainProfileLocation = baseUrl + '/satellit/profiles/';
var satellite1 = baseUrl + '/satellit/profiles/1/';
var satellite2 = baseUrl + '/satellit/profiles/2/';
var satellite3 = baseUrl + '/satellit/profiles/3/';
var resultUrlPattern = /^http:\/\/mister-x.me\/satellit\/editdesc\/(\d{6,10})\//gi
var urlPattern = /\/satellit\/editdesc\/([\d]{6,10})\/\?type\=[0]/gi
var poolInterval = 5;

chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
	if(msg.text) {
		if (msg.text == "report_back") {
			   var listOfLinks = document.getElementsByTagName('a');
				var results = [];
				if (listOfLinks.length > 0) {
					for (index = 0; index < listOfLinks.length; index++) {
						var currentTestElement = listOfLinks[index];
						var hrefAttribute = currentTestElement.getAttribute('href');
						if (hrefAttribute.match(urlPattern)) {
							var completteUrl = baseUrl + hrefAttribute;
							results.push(completteUrl);
						}
					}
				}
			sendResponse(results);
		}
		if(msg.text == "toSat2") {
			var node = document.createElement("meta");
			node.setAttribute("http-equiv", "refresh");
			node.setAttribute("content", poolInterval + ";"+satellite2);
			var header = document.getElementsByTagName("head");
			header[0].appendChild(node);
		}
		if(msg.text == "toSat3") {
			var node = document.createElement("meta");
			node.setAttribute("http-equiv", "refresh");
			node.setAttribute("content", poolInterval + ";"+satellite3);
			var header = document.getElementsByTagName("head");
			header[0].appendChild(node);
		}
		if(msg.text == "toSat1") {
			var node = document.createElement("meta");
			node.setAttribute("http-equiv", "refresh");
			node.setAttribute("content", poolInterval + ";"+satellite1);
			var header = document.getElementsByTagName("head");
			header[0].appendChild(node);
		}
		if(msg.text == "WriteDescription") {
			var textField = document.getElementById("description");
			var originalText = textField.innerHTML
			textField.innerHTML = originalText + "Занято!";
			var injectedCode = 'lock()';
			var script = document.createElement('script');
			script.appendChild(document.createTextNode('('+ injectedCode +')();'));
			(document.body || document.head || document.documentElement).appendChild(script);;
		}
	}
});